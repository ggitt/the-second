package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description：
 *
 * @author ShengfengLin
 * @date Created on 2020/5/18
 */
@RestController
public class helloController {

    @RequestMapping("hello")
    public String hello(){
        return "hello";
    }
}
