package com.example.demo.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Description：
 *
 * @author ShengfengLin
 * @date Created on 2020/5/21
 */
@Component
@ConfigurationProperties(prefix = "person")
public class person {
    private String name;
    private Map<String,Object> map;
    private List<String> list;

    public person() {

    }

    public person(String name, Map map, List list) {
        this.name = name;
        this.map = map;
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "person{" +
                "name='" + name + '\'' +
                ", map=" + map +
                ", list=" + list +
                '}';
    }
}
