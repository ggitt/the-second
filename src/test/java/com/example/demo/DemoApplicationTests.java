package com.example.demo;

import com.example.demo.pojo.dog;
import com.example.demo.pojo.person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    private person person;

    @Test
    void contextLoads() {
        System.out.println(person);
    }

}
